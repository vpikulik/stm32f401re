
#include <stdio.h>
#include <stdlib.h>

#include "stm32f4xx_hal.h"

#include "cmsis_os.h"

#include "uip.h"
#include "uip_arp.h"

//буфер, нужный для UIP
//*********************************************
#define BUF ((struct uip_eth_hdr *)&uip_buf[0])
//*********************************************


static void MainTaskHandler(void const *argument);
void uIP(void const *argument);
void uIP_periodic(void const *argument);


int main(void) {

    HAL_Init();
    printf("init hal\n");

    struct uip_eth_addr mac = { { 0x00, 0xa2, 0xb5, 0x34, 0x43, 0x67 } };

    //проинитим SPI
    enc28j60SpiInit();
    printf("init spi\n");

    //проинитим наш  enc28j60
    enc28j60Init(mac.addr);
    printf("init enc28j60\n");

    // инициализация стека
    uip_init();
    printf("init uip\n");
    uip_arp_init();
    printf("init arp\n");

    // установим наш МАС
    uip_setethaddr(mac);
    printf("set mac\n");

    // установим адрес хоста (не используем dhcp)
    // наш хост будет доступен по адресу 192.168.0.100
    uip_ipaddr_t ipaddr;
    uip_ipaddr(ipaddr, 192, 168, 0, 100);
    uip_sethostaddr(ipaddr);
    uip_ipaddr(ipaddr, 192, 168, 0, 1);
    uip_setdraddr(ipaddr);
    uip_ipaddr(ipaddr, 255, 255, 255, 0);
    uip_setnetmask(ipaddr);
    printf("set ip4\n");

    hello_world_init();
    printf("httpd init\n");

    // osThreadDef(MainTask, MainTaskHandler, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
    // osThreadCreate(osThread(MainTask), NULL);

    osThreadDef(Task_uIP, uIP, osPriorityAboveNormal, 0, 1024);
    osThreadCreate(osThread(Task_uIP), NULL);

    osThreadDef(Task_uIP_periodic, uIP_periodic, osPriorityNormal, 0, 1024);
    osThreadCreate(osThread(Task_uIP_periodic), NULL);

    /* Start scheduler */
    osKernelStart();

    /* We should never get here as control is now taken by the scheduler */
    for(;;);
}

static void MainTaskHandler(void const *argument)
{
    (void) argument;

    while (1) {
        printf("Main task\n");
        osDelay(100);
    }
}

void SysTick_Handler(void)
{
    osSystickHandler();
}
 
void uIP_periodic(void const *argument)
{
    printf("vTask_uIP_periodic\n");
    uint32_t i;
    uint8_t delay_arp = 0;

    while(1)
    {
        osDelay(configTICK_RATE_HZ/2); // полсекунды
        delay_arp++;
        for (i = 0; i < UIP_CONNS; i++) {
            uip_periodic(i);
            if (uip_len > 0) {
                uip_arp_out();
                enc28j60PacketSend(uip_len,(uint8_t *) uip_buf);
            }
        }

        #if UIP_UDP
        for(i = 0; i < UIP_UDP_CONNS; i++) {
            uip_udp_periodic(i);
            if(uip_len > 0) {
                uip_arp_out();
                network_send();
            }
        }
        #endif /* UIP_UDP */

        if (delay_arp >= 20) 
        { // один раз за 20 проходов цикла, около 10 сек.
            delay_arp = 0;
            uip_arp_timer();
        }
        osThreadYield();
    }
}
//--------------------------------------------------------------
void uIP(void const *argument) 
{
    printf("vTask_uIP\n");
    while(1)
    {
        uip_len = enc28j60PacketReceive(UIP_BUFSIZE, (uint8_t *) uip_buf);
        if (uip_len > 0) 
        {        
            if (BUF->type == htons(UIP_ETHTYPE_IP)) 
            {
                uip_arp_ipin();
                uip_input();
                if (uip_len > 0) {
                  uip_arp_out();
                  enc28j60PacketSend(uip_len,(uint8_t *) uip_buf);
                }
            } else if (BUF->type == htons(UIP_ETHTYPE_ARP)) 
            {
                uip_arp_arpin();
                if (uip_len > 0) {
                    enc28j60PacketSend(uip_len,(uint8_t *) uip_buf);
                }
            }
        }
        osDelay(1);
    }
}