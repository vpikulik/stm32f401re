
ifndef PROGRAM
PROGRAM = main
endif

ifndef CUR_DIR
CUR_DIR = $(shell pwd)
endif
ifndef CUBE_DIR
CUBE_DIR = /usr/local/src/stm32cubef4lite
endif
ifndef HAL_DIR
HAL_DIR = $(CUBE_DIR)/Drivers/STM32F4xx_HAL_Driver/Src
endif
ifndef BUILD_DIR
BUILD_DIR = $(CUR_DIR)/__build
endif

DEF_INCLUDE = -I$(CUR_DIR)

INCLUDE += -I$(CUBE_DIR)/Drivers/CMSIS/Include
INCLUDE += -I$(CUBE_DIR)/Drivers/STM32F4xx_HAL_Driver/Inc
INCLUDE += -I$(CUBE_DIR)/Drivers/CMSIS/Device/ST/STM32F4xx/Include
INCLUDE += -I$(CUBE_DIR)/Drivers/BSP/STM32F401-Discovery
INCLUDE += -I$(CUBE_DIR)/Drivers/BSP/STM32F4xx-Nucleo

FULL_INCLUDE = $(DEF_INCLUDE) $(INCLUDE)

ifndef LD_FILE
LD_FILE = $(CUBE_DIR)/ld/STM32F401RE_FLASH.ld
endif

STARTUP_FILE = $(CUBE_DIR)/Drivers/CMSIS/Device/ST/STM32F4xx/Source/Templates/gcc/startup_stm32f401xe.s

SRCS += $(CUBE_DIR)/Drivers/CMSIS/Device/ST/STM32F4xx/Source/Templates/system_stm32f4xx.c
SRCS += $(STARTUP_FILE)

get_md5 = $(strip $(subst -,,$(shell echo $(1) | md5sum)))
get_path_md5 = $(call get_md5,$(dir $(abspath $(1))))
get_target = $(addprefix $(BUILD_DIR)/,$(addprefix $(call get_path_md5,$(1)), $(addprefix /,$(addsuffix .o,$(basename $(notdir $(1)))))))
get_targets = $(foreach fl,$(1),$(call get_target, $(fl)))

ASM_SRCS = $(filter %.s,$(SRCS))
CC_SRCS = $(filter %.c,$(SRCS))
TARGETS = $(call get_targets,$(SRCS))
ASM_TARGETS = $(call get_targets,$(ASM_SRCS))
CC_TARGETS = $(call get_targets,$(CC_SRCS))
OBJECTS = $(TARGETS)

CC = arm-none-eabi-gcc
ASM = arm-none-eabi-as

ifndef PARAMS
PARAMS = USE_HAL_DRIVER
PARAMS += STM32F401xE
PARAMS += USE_STM32F4XX_NUCLE
endif

DVARS = $(addprefix -D,$(PARAMS))

COMPILER_OPTS = -mcpu=cortex-m4 -mlittle-endian -mthumb
ifdef DEBUG
COMPILER_OPTS += -g
endif

GCC_OPTIONS = $(COMPILER_OPTS) --specs=rdimon.specs -lc -lrdimon -mfpu=fpv4-sp-d16 -mfloat-abi=softfp
ASM_OPTIONS = $(COMPILER_OPTS)


all: $(PROGRAM).hex

clean:
	rm -rf *.o *.elf *.bin *.hex $(BUILD_DIR)

define MAKE_CC_TARGET
$2:
	mkdir -p $(dir $2)
	$(CC) $(GCC_OPTIONS) $(FULL_INCLUDE) $(DVARS) -o $2 -c $1
endef
$(foreach sfl,$(CC_SRCS),$(eval $(call MAKE_CC_TARGET,$(sfl),$(call get_target,$(sfl)))))

define MAKE_ASM_TARGET
$2:
	mkdir -p $(dir $2)
	$(ASM) $(ASM_OPTIONS) $(FULL_INCLUDE) -o $2 -c $1
endef
$(foreach sfl,$(ASM_SRCS),$(eval $(call MAKE_ASM_TARGET,$(sfl),$(call get_target,$(sfl)))))

$(PROGRAM).elf: $(OBJECTS)
	$(CC) $(GCC_OPTIONS) -mthumb-interwork $(DVARS) -T$(LD_FILE) -Wl,--gc-sections $(OBJECTS) -o $(PROGRAM).elf

$(PROGRAM).hex: $(PROGRAM).elf
	arm-none-eabi-objcopy -Oihex $(PROGRAM).elf $(PROGRAM).hex

gdb: $(PROGRAM).elf
	arm-none-eabi-gdb -ex="target ext localhost:3333" -ex "mon reset halt" -ex "mon arm semihosting enable" $(PROGRAM).elf

flash: $(PROGRAM).hex
	(echo "reset halt"; echo "flash write_image erase $(PROGRAM).hex"; sleep 1; echo "reset run" ) | telnet localhost 4444

openocd:
	openocd -f /usr/share/openocd/scripts/board/st_nucleo_f401re.cfg

show_startup:
	vim $(STARTUP_FILE)