
#include <stdio.h>

#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"


#define DAY_SECONDS 60*60*24
#define CAP_VER2_00 (1<<0)
#define CAP_SDHC    (1<<1)
#define spi_cs_low() HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
#define spi_cs_high() HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
#define sd_get_r3 sd_get_r7

SPI_HandleTypeDef Spi1Handle;

enum sd_speed { SD_SPEED_INVALID, SD_SPEED_400KHZ, SD_SPEED_25MHZ };

typedef struct hwif {
    int initialized;
    int sectors;
    int erase_sectors;
    int capabilities;
} hwif;


/*
 * Code is split into 3 parts:
 * - generic SPI code: adapt for your MCU
 * - sd card code, with crc7 and crc16 calculations
 *   there's lots of it, but it's simple
 * - fatfs interface. If you use anything else, look here for
 *   interface to SD card code
 */

/*** spi functions ***/
static void spi_init(void);
static void spi_set_speed(enum sd_speed speed);
static uint8_t spi_txrx(uint8_t data);

/* crc helpers */
static uint8_t crc7_one(uint8_t t, uint8_t data);
uint8_t crc7(const uint8_t *p, int len);
static uint16_t crc16_ccitt(uint16_t crc, uint8_t ser_data);
uint16_t crc16(const uint8_t *p, int len);

/*** sd functions - on top of spi code ***/
static void sd_cmd(uint8_t cmd, uint32_t arg);
static uint8_t sd_get_r1();
static uint16_t sd_get_r2();
static uint8_t sd_get_r7(uint32_t *r7);

static void print_r1(uint8_t r);
static void print_r2(uint16_t r);

static void sd_nec();
static int sd_init(hwif *hw);
static int sd_read_status(hwif *hw);
static int sd_get_data(hwif *hw, uint8_t *buf, int len);
static int sd_put_data(hwif *hw, const uint8_t *buf, int len);
static int sd_read_csd(hwif *hw);
static int sd_read_cid(hwif *hw);
static int sd_readsector(hwif *hw, uint32_t address, uint8_t *buf);
static int sd_writesector(hwif *hw, uint32_t address, const uint8_t *buf);

/*** public API - on top of sd/spi code ***/
int hwif_init(hwif* hw);
int sd_read(hwif* hw, uint32_t address, uint8_t *buf);
int sd_write(hwif* hw, uint32_t address,const uint8_t *buf);
