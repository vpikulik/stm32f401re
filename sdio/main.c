

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_pwr.h"
#include "stm32f4xx_hal_rcc_ex.h"

#include "ff_gen_drv.h"
#include "sd_diskio.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
static void SystemClock_Config(void);

FATFS FatFs;

int main(void)
{
    HAL_Init();
    SystemClock_Config();

    FIL fil;       /* File object */
    char line[82]; /* Line buffer */
    char message[] = "Hello world!!! SDIO said this)";
    FRESULT fr;    /* FatFs return code */
    UINT res_len;

    if(FATFS_LinkDriver(&SD_Driver, 0) == 0)
    {
        printf("Read\n");
        f_mount(&FatFs, "", 0);
        fr = f_open(&fil, "message.txt", FA_READ);
        while (f_gets(line, sizeof line, &fil)) {
            printf(line);
        }
        f_close(&fil);
        f_mount(NULL, "", 0);

        printf("Write\n");
        f_mount(&FatFs, "", 0);
        fr = f_open(&fil, "res.txt", FA_CREATE_ALWAYS | FA_WRITE);
        fr = f_write(&fil, message, sizeof message, &res_len);
        printf("size: %i, %i, %s\n", sizeof message, res_len, message);
        f_close(&fil);
        f_mount(NULL, "", 0);
    }

    while (1) {

    }
}

void SysTick_Handler(void)
{
    HAL_IncTick();
}

void DMA2_Stream3_IRQHandler(void)
{
    printf("DMA RX\n");
    BSP_SD_DMA_Rx_IRQHandler();
}

void DMA2_Stream6_IRQHandler(void)
{
    printf("DMA TX\n");
    BSP_SD_DMA_Tx_IRQHandler();
}

void SDIO_IRQHandler(void)
{
    printf("SDIO int\n");
    BSP_SD_IRQHandler();
}

static void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;

    /* Enable Power Control clock */
    __HAL_RCC_PWR_CLK_ENABLE();

    /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    /* Enable HSE Oscillator and activate PLL with HSE as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = 8;
    RCC_OscInitStruct.PLL.PLLN = 336;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
    RCC_OscInitStruct.PLL.PLLQ = 7;
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;  
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
}
