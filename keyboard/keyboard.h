
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"

#define BUTTON_STATUS(__NAME__, __PORT__, __PIN__) Button __NAME__ = {__PORT__, __PIN__, 0, 0};

typedef struct {
    GPIO_TypeDef* port;
    uint16_t pin;
    uint8_t active;
    uint32_t counter;
} Button;

typedef struct {
    int8_t count;
    Button **buttons;
} Keyboard;

void keyboard_button_it(Button *button);
void keyboard_clicked(Button *button);
void keyboard_check_status(Keyboard *keyboard);
