
#include "keyboard.h"


void keyboard_button_it(Button *button) {
    if (button->active == 0) {
        button->counter = 1;
        button->active = 1;
    }
}

void keyboard_check_status(Keyboard *keyboard) {
    int8_t i;
    for (i=0; i<keyboard->count; i++) {
        Button *button = keyboard->buttons[i];

        if (button->active == 1) {
            if (HAL_GPIO_ReadPin(button->port, button->pin) == GPIO_PIN_SET) {
                button->counter ++;
            }
            else {
                button->counter = 0;
                button->active = 0;
            }

            if (button->counter == 4) {
                button->counter = 0;
                button->active = 0;
                keyboard_clicked(button);
            }
        }
    }
}


