#!/usr/bin/env bash

# link project folder
ln -s /vagrant stm32f401

# apt package
sudo apt-get update -qq
sudo apt-get install tmux vim -qq
sudo apt-get install gcc-arm-none-eabi gdb-arm-none-eabi openocd -qq

# install stm32cubef4
#wget -O /tmp/stm32cubef4.zip http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/firmware/stm32cubef4.zip
wget -O /tmp/stm32cubef4lite.zip https://www.dropbox.com/s/ck75xt61bhlsstu/stm32cubef4lite.zip?dl=1
unzip -d /tmp/stm32cubef4lite /tmp/stm32cubef4lite.zip
sudo mv /tmp/stm32cubef4lite/STM32Cube_FW_F4_V1.8.0 /usr/local/src/stm32cubef4lite
rm -rf /tmp/stm32cubef4lite.zip
