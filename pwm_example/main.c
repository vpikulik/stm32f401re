

/* Includes ------------------------------------------------------------------*/
#include  <stdio.h>

#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"
#include "stm32f4xx_hal_tim.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

#define PERIOD_MIN 199
#define PERIOD_MAX 999
#define PERIOD_STEP 100
#define PULSE_MIN 100
#define PULSE_MAX 500
#define PULSE_STEP 50


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile uint16_t led_status = 0;
volatile uint16_t pwm_period = PERIOD_MIN;
volatile uint16_t pwm_pulse = PULSE_MIN;

TIM_HandleTypeDef Tim2_config;
TIM_OC_InitTypeDef Tim2_pwm_config;

/* Private function prototypes -----------------------------------------------*/
inline void init_led();
inline void init_button();
inline void init_pwm();
inline void start_pwm();
inline void stop_pwm();
inline void change_frequency();

/* Private functions ---------------------------------------------------------*/


int main(void)
{
   
    HAL_Init();

    init_led();
    init_button();
    init_pwm();
    start_pwm();

    while (1) {
    }
}

inline void init_led() {
    GPIO_InitTypeDef  GPIO_Led_Struct;
    __GPIOA_CLK_ENABLE();
    GPIO_Led_Struct.Pin = GPIO_PIN_5;
    GPIO_Led_Struct.Mode = GPIO_MODE_AF_PP;
    GPIO_Led_Struct.Alternate = GPIO_AF1_TIM2;
    GPIO_Led_Struct.Speed = GPIO_SPEED_FAST;
    HAL_GPIO_Init(GPIOA, &GPIO_Led_Struct);
    // HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
    // GPIO_Led_Struct.Pull = GPIO_NOPULL;
}

inline void init_button() {
    GPIO_InitTypeDef GPIO_Button_Struct;
    __GPIOC_CLK_ENABLE();
    GPIO_Button_Struct.Pin = GPIO_PIN_13;
    GPIO_Button_Struct.Pull = GPIO_NOPULL;
    GPIO_Button_Struct.Mode = GPIO_MODE_IT_FALLING;
    HAL_GPIO_Init(GPIOC, &GPIO_Button_Struct);
    HAL_NVIC_SetPriority((IRQn_Type)(EXTI15_10_IRQn), 0, 0x00);
    HAL_NVIC_EnableIRQ((IRQn_Type)(EXTI15_10_IRQn));
}

inline void init_pwm() {
    __HAL_RCC_TIM2_CLK_ENABLE();

    RCC_ClkInitTypeDef sClokConfig;
    uint32_t pFLatency;
    uint32_t uwTimclock, uwAPB1Prescaler = 0;

    HAL_RCC_GetClockConfig(&sClokConfig, &pFLatency);
    uwAPB1Prescaler = sClokConfig.APB1CLKDivider;
    if (uwAPB1Prescaler == 0) {
        uwTimclock = HAL_RCC_GetPCLK1Freq();
    }
    else {
        uwTimclock = 2*HAL_RCC_GetPCLK1Freq();
    }

    Tim2_config.Instance = TIM2;
    Tim2_config.Init.Period = pwm_period;
    Tim2_config.Init.Prescaler = (uint32_t) ((uwTimclock / 1000) - 1); // 1khz
    Tim2_config.Init.ClockDivision = 0;
    Tim2_config.Init.CounterMode = TIM_COUNTERMODE_UP;
    // Tim2_config.Init.RepetitionCounter = 0;

    HAL_TIM_PWM_Init(&Tim2_config);

    Tim2_pwm_config.OCMode = TIM_OCMODE_PWM1;
    Tim2_pwm_config.OCIdleState = TIM_CCx_ENABLE;
    Tim2_pwm_config.Pulse = pwm_pulse;
    Tim2_pwm_config.OCPolarity = TIM_OCPOLARITY_HIGH;

    HAL_TIM_PWM_ConfigChannel(&Tim2_config, &Tim2_pwm_config, TIM_CHANNEL_1);
}

inline void deinit_pwm() {
    HAL_TIM_PWM_DeInit(&Tim2_config);
}


inline void start_pwm(){
    HAL_TIM_PWM_Start(&Tim2_config, TIM_CHANNEL_1);
}


inline void stop_pwm() {
    HAL_TIM_PWM_Stop(&Tim2_config, TIM_CHANNEL_1);
}


inline void change_frequency() {
    if (pwm_period < PERIOD_MAX) {
        pwm_period += PERIOD_STEP;
        pwm_pulse += PULSE_STEP;
    } else {
        pwm_period = PERIOD_MIN;
        pwm_pulse = PULSE_MIN;
    }
    stop_pwm();
    __HAL_TIM_SET_COUNTER(&Tim2_config, 0);
    __HAL_TIM_SET_AUTORELOAD(&Tim2_config, pwm_period);
    __HAL_TIM_SET_COMPARE(&Tim2_config, TIM_CHANNEL_1, pwm_pulse);
    start_pwm();
    printf("PWM changed: %i, %i\n", pwm_period, pwm_pulse);
}


void EXTI15_10_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == GPIO_PIN_13)
    {
        change_frequency();
    }
}
