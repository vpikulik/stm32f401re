

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"
#include "stm32f4xx_hal_tim.h"

#include "keyboard.h"
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile int32_t led_status = 1;
TIM_HandleTypeDef Tim4_config;
Keyboard keyboard;

/* Private function prototypes -----------------------------------------------*/
void init_led();
void init_button();
void init_timer();
inline void toggle_led();

/* Private functions ---------------------------------------------------------*/
BUTTON_STATUS(button1, GPIOB, GPIO_PIN_3);
BUTTON_STATUS(button2, GPIOB, GPIO_PIN_5);


int main(void)
{
   
    HAL_Init();

    keyboard.count = 2;
    keyboard.buttons = malloc(sizeof(Button*)*2);
    keyboard.buttons[0] = &button1;
    keyboard.buttons[1] = &button2;

    init_led();
    init_button();
    init_timer();
    // init_pwm();
    // start_pwm();

    while (1) {
    }
}

inline void init_led() {
    printf("Init led\n");
    GPIO_InitTypeDef  GPIO_Led_Struct;
    __GPIOA_CLK_ENABLE();
    GPIO_Led_Struct.Pin = GPIO_PIN_5;
    GPIO_Led_Struct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_Led_Struct.Alternate = GPIO_AF1_TIM2;
    GPIO_Led_Struct.Speed = GPIO_SPEED_FAST;
    HAL_GPIO_Init(GPIOA, &GPIO_Led_Struct);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
}

inline void init_button() {
    printf("Init button\n");
    GPIO_InitTypeDef GPIO_Button_Struct;
    __GPIOB_CLK_ENABLE();
    GPIO_Button_Struct.Pin = GPIO_PIN_3 | GPIO_PIN_5;
    GPIO_Button_Struct.Pull = GPIO_PULLDOWN;
    GPIO_Button_Struct.Mode = GPIO_MODE_IT_RISING;
    GPIO_Button_Struct.Speed = GPIO_SPEED_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_Button_Struct);

    HAL_NVIC_SetPriority((IRQn_Type)(EXTI3_IRQn), 0, 0x00);
    HAL_NVIC_EnableIRQ((IRQn_Type)(EXTI3_IRQn));
    HAL_NVIC_SetPriority((IRQn_Type)(EXTI9_5_IRQn), 1, 0x00);
    HAL_NVIC_EnableIRQ((IRQn_Type)(EXTI9_5_IRQn));
}

void toggle_led() {
    if (led_status) {
        led_status = 0;
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
    } else {
        led_status = 1;
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
    }
}


void init_timer() {
    __HAL_RCC_TIM4_CLK_ENABLE();

    RCC_ClkInitTypeDef sClokConfig;
    uint32_t pFLatency;
    uint32_t uwTimclock, uwAPB1Prescaler = 0;

    HAL_RCC_GetClockConfig(&sClokConfig, &pFLatency);
    uwAPB1Prescaler = sClokConfig.APB1CLKDivider;
    if (uwAPB1Prescaler == 0) {
        uwTimclock = HAL_RCC_GetPCLK1Freq();
    }
    else {
        uwTimclock = 2*HAL_RCC_GetPCLK1Freq();
    }


    Tim4_config.Instance = TIM4;
    Tim4_config.Init.Period = 24;
    Tim4_config.Init.Prescaler = (uint32_t) ((uwTimclock / 1000) - 1); // 1khz
    Tim4_config.Init.ClockDivision = 0;
    Tim4_config.Init.CounterMode = TIM_COUNTERMODE_UP;
    // Tim4_config.Init.RepetitionCounter = 0;

    HAL_TIM_Base_Init(&Tim4_config);

    HAL_NVIC_SetPriority(TIM4_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ((IRQn_Type)(TIM4_IRQn));

    HAL_TIM_Base_Start_IT(&Tim4_config);
}


void EXTI3_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}

void EXTI9_5_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5);
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (button1.pin == GPIO_Pin)
    {
        keyboard_button_it(&button1);
    } else if (button2.pin == GPIO_Pin)
    {
        keyboard_button_it(&button2);
    }
}

void TIM4_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&Tim4_config);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    keyboard_check_status(&keyboard);

}

void keyboard_clicked(Button *button) {
    printf("Button was clicked\n");
    if (button->pin == GPIO_PIN_3) {
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
    } else if (button->pin == GPIO_PIN_5) {
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
    }
}
