

/* Includes ------------------------------------------------------------------*/
#include  <stdio.h>

#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"
#include "stm32f4xx_hal_spi.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

char src1[18] = "Hello from Master!";
char res1[18];

char src2[18] = "Hello from Slave!!";
char res2[18];

SPI_HandleTypeDef Spi1Handle;
SPI_HandleTypeDef Spi2Handle;
SPI_HandleTypeDef Spi3Handle;
DMA_HandleTypeDef spi1_hdma_tx;
DMA_HandleTypeDef spi1_hdma_rx;
DMA_HandleTypeDef spi2_hdma_tx;
DMA_HandleTypeDef spi2_hdma_rx;
DMA_HandleTypeDef spi3_hdma_tx;
DMA_HandleTypeDef spi3_hdma_rx;
GPIO_InitTypeDef  GPIO_InitStruct;

/* Private function prototypes -----------------------------------------------*/
inline void init_button();
inline void init_spi1();
inline void init_spi2();
inline void init_spi3();
inline start_spi();


/* Private functions ---------------------------------------------------------*/


int main(void)
{
   
    HAL_Init();

    init_button();
    init_spi1();
    // init_spi2();
    init_spi3();
    printf("Init.\n");

    while (1) {
    }
}

inline start_spi() {

    if (HAL_SPI_Transmit_DMA(&Spi3Handle, (uint8_t*)src1, 18) != HAL_OK) {
        printf("Error spi3\n");
    } else printf("Start spi3\n");

    if (HAL_SPI_Receive_DMA(&Spi1Handle, (uint8_t*)res1, 18) != HAL_OK) {
        printf("Error spi1\n");
    } else printf("Start spi1\n");
}

inline void init_spi1() {
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_SPI1_CLK_ENABLE();
    __HAL_RCC_DMA2_CLK_ENABLE();

    GPIO_InitStruct.Pin       = GPIO_PIN_5;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull      = GPIO_PULLUP;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = GPIO_PIN_6;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    Spi1Handle.Instance               = SPI1;
    Spi1Handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
    Spi1Handle.Init.Direction         = SPI_DIRECTION_2LINES;
    Spi1Handle.Init.CLKPhase          = SPI_PHASE_1EDGE;
    Spi1Handle.Init.CLKPolarity       = SPI_POLARITY_HIGH;
    Spi1Handle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
    Spi1Handle.Init.CRCPolynomial     = 7;
    Spi1Handle.Init.DataSize          = SPI_DATASIZE_8BIT;
    Spi1Handle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
    Spi1Handle.Init.NSS               = SPI_NSS_SOFT;
    Spi1Handle.Init.TIMode            = SPI_TIMODE_DISABLE;
    Spi1Handle.Init.Mode = SPI_MODE_MASTER;

    spi1_hdma_tx.Instance                 = DMA2_Stream3;
    spi1_hdma_tx.Init.Channel             = DMA_CHANNEL_3;
    spi1_hdma_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
    spi1_hdma_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
    spi1_hdma_tx.Init.MemInc              = DMA_MINC_ENABLE;
    spi1_hdma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    spi1_hdma_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
    spi1_hdma_tx.Init.Mode                = DMA_NORMAL;
    spi1_hdma_tx.Init.Priority            = DMA_PRIORITY_LOW;
    spi1_hdma_tx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
    spi1_hdma_tx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
    spi1_hdma_tx.Init.MemBurst            = DMA_MBURST_INC4;
    spi1_hdma_tx.Init.PeriphBurst         = DMA_PBURST_INC4;
    HAL_DMA_Init(&spi1_hdma_tx);
    __HAL_LINKDMA(&Spi1Handle, hdmatx, spi1_hdma_tx);

    spi1_hdma_rx.Instance                 = DMA2_Stream2;
    spi1_hdma_rx.Init.Channel             = DMA_CHANNEL_3;
    spi1_hdma_rx.Init.Direction           = DMA_PERIPH_TO_MEMORY;
    spi1_hdma_rx.Init.PeriphInc           = DMA_PINC_DISABLE;
    spi1_hdma_rx.Init.MemInc              = DMA_MINC_ENABLE;
    spi1_hdma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    spi1_hdma_rx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
    spi1_hdma_rx.Init.Mode                = DMA_NORMAL;
    spi1_hdma_rx.Init.Priority            = DMA_PRIORITY_HIGH;
    spi1_hdma_rx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
    spi1_hdma_rx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
    spi1_hdma_rx.Init.MemBurst            = DMA_MBURST_INC4;
    spi1_hdma_rx.Init.PeriphBurst         = DMA_PBURST_INC4;
    HAL_DMA_Init(&spi1_hdma_rx);
    __HAL_LINKDMA(&Spi1Handle, hdmarx, spi1_hdma_rx);

    HAL_NVIC_SetPriority(DMA2_Stream2_IRQn, 0, 2);
    HAL_NVIC_EnableIRQ(DMA2_Stream2_IRQn);
    HAL_NVIC_SetPriority(DMA2_Stream3_IRQn, 0, 1);
    HAL_NVIC_EnableIRQ(DMA2_Stream3_IRQn);


    if (HAL_SPI_Init(&Spi1Handle) == HAL_OK) printf("spi1 init ok\n");
    else printf("spi1 init fail\n");
}

inline void init_spi2() {

    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_SPI2_CLK_ENABLE();
    __HAL_RCC_DMA1_CLK_ENABLE();

    GPIO_InitStruct.Pin       = GPIO_PIN_13;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull      = GPIO_PULLUP;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = GPIO_PIN_14;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = GPIO_PIN_15;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    Spi2Handle.Instance               = SPI2;
    Spi2Handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
    Spi2Handle.Init.Direction         = SPI_DIRECTION_2LINES;
    Spi2Handle.Init.CLKPhase          = SPI_PHASE_1EDGE;
    Spi2Handle.Init.CLKPolarity       = SPI_POLARITY_HIGH;
    Spi2Handle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
    Spi2Handle.Init.CRCPolynomial     = 7;
    Spi2Handle.Init.DataSize          = SPI_DATASIZE_8BIT;
    Spi2Handle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
    Spi2Handle.Init.NSS               = SPI_NSS_SOFT;
    Spi2Handle.Init.TIMode            = SPI_TIMODE_DISABLE;
    Spi2Handle.Init.Mode = SPI_MODE_MASTER;

    spi2_hdma_tx.Instance                 = DMA1_Stream4;
    spi2_hdma_tx.Init.Channel             = DMA_CHANNEL_0;
    spi2_hdma_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
    spi2_hdma_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
    spi2_hdma_tx.Init.MemInc              = DMA_MINC_ENABLE;
    spi2_hdma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    spi2_hdma_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
    spi2_hdma_tx.Init.Mode                = DMA_NORMAL;
    spi2_hdma_tx.Init.Priority            = DMA_PRIORITY_LOW;
    spi2_hdma_tx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
    spi2_hdma_tx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
    spi2_hdma_tx.Init.MemBurst            = DMA_MBURST_INC4;
    spi2_hdma_tx.Init.PeriphBurst         = DMA_PBURST_INC4;
    HAL_DMA_Init(&spi2_hdma_tx);
    __HAL_LINKDMA(&Spi2Handle, hdmatx, spi2_hdma_tx);

    spi2_hdma_rx.Instance                 = DMA1_Stream3;
    spi2_hdma_rx.Init.Channel             = DMA_CHANNEL_0;
    spi2_hdma_rx.Init.Direction           = DMA_PERIPH_TO_MEMORY;
    spi2_hdma_rx.Init.PeriphInc           = DMA_PINC_DISABLE;
    spi2_hdma_rx.Init.MemInc              = DMA_MINC_ENABLE;
    spi2_hdma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    spi2_hdma_rx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
    spi2_hdma_rx.Init.Mode                = DMA_NORMAL;
    spi2_hdma_rx.Init.Priority            = DMA_PRIORITY_HIGH;
    spi2_hdma_rx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
    spi2_hdma_rx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
    spi2_hdma_rx.Init.MemBurst            = DMA_MBURST_INC4;
    spi2_hdma_rx.Init.PeriphBurst         = DMA_PBURST_INC4;
    HAL_DMA_Init(&spi2_hdma_rx);
    __HAL_LINKDMA(&Spi2Handle, hdmarx, spi2_hdma_rx);

    HAL_NVIC_SetPriority(DMA1_Stream4_IRQn, 0, 2);
    HAL_NVIC_EnableIRQ(DMA1_Stream4_IRQn);
    HAL_NVIC_SetPriority(DMA1_Stream3_IRQn, 0, 1);
    HAL_NVIC_EnableIRQ(DMA1_Stream3_IRQn);

    if (HAL_SPI_Init(&Spi2Handle) == HAL_OK) printf("spi2 init ok\n");
    else printf("spi2 init fail\n");
}

inline void init_spi3() {

    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_SPI3_CLK_ENABLE();
    __HAL_RCC_DMA1_CLK_ENABLE();

    GPIO_InitStruct.Pin       = GPIO_PIN_10;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull      = GPIO_PULLUP;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
    GPIO_InitStruct.Alternate = GPIO_AF6_SPI3;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = GPIO_PIN_12;
    GPIO_InitStruct.Alternate = GPIO_AF6_SPI3;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = GPIO_PIN_11;
    GPIO_InitStruct.Alternate = GPIO_AF6_SPI3;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    Spi3Handle.Instance               = SPI3;
    Spi3Handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
    Spi3Handle.Init.Direction         = SPI_DIRECTION_2LINES;
    Spi3Handle.Init.CLKPhase          = SPI_PHASE_1EDGE;
    Spi3Handle.Init.CLKPolarity       = SPI_POLARITY_HIGH;
    Spi3Handle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
    Spi3Handle.Init.CRCPolynomial     = 7;
    Spi3Handle.Init.DataSize          = SPI_DATASIZE_8BIT;
    Spi3Handle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
    Spi3Handle.Init.NSS               = SPI_NSS_SOFT;
    Spi3Handle.Init.TIMode            = SPI_TIMODE_DISABLE;
    Spi3Handle.Init.Mode = SPI_MODE_SLAVE;

    spi3_hdma_tx.Instance                 = DMA1_Stream5;
    spi3_hdma_tx.Init.Channel             = DMA_CHANNEL_0;
    spi3_hdma_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
    spi3_hdma_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
    spi3_hdma_tx.Init.MemInc              = DMA_MINC_ENABLE;
    spi3_hdma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    spi3_hdma_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
    spi3_hdma_tx.Init.Mode                = DMA_NORMAL;
    spi3_hdma_tx.Init.Priority            = DMA_PRIORITY_LOW;
    spi3_hdma_tx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
    spi3_hdma_tx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
    spi3_hdma_tx.Init.MemBurst            = DMA_MBURST_INC4;
    spi3_hdma_tx.Init.PeriphBurst         = DMA_PBURST_INC4;
    HAL_DMA_Init(&spi3_hdma_tx);
    __HAL_LINKDMA(&Spi3Handle, hdmatx, spi3_hdma_tx);

    spi3_hdma_rx.Instance                 = DMA1_Stream2;
    spi3_hdma_rx.Init.Channel             = DMA_CHANNEL_0;
    spi3_hdma_rx.Init.Direction           = DMA_PERIPH_TO_MEMORY;
    spi3_hdma_rx.Init.PeriphInc           = DMA_PINC_DISABLE;
    spi3_hdma_rx.Init.MemInc              = DMA_MINC_ENABLE;
    spi3_hdma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    spi3_hdma_rx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
    spi3_hdma_rx.Init.Mode                = DMA_NORMAL;
    spi3_hdma_rx.Init.Priority            = DMA_PRIORITY_HIGH;
    spi3_hdma_rx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
    spi3_hdma_rx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
    spi3_hdma_rx.Init.MemBurst            = DMA_MBURST_INC4;
    spi3_hdma_rx.Init.PeriphBurst         = DMA_PBURST_INC4;
    HAL_DMA_Init(&spi3_hdma_rx);
    __HAL_LINKDMA(&Spi3Handle, hdmarx, spi3_hdma_rx);

    HAL_NVIC_SetPriority(DMA1_Stream2_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Stream2_IRQn);
    HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);

    if (HAL_SPI_Init(&Spi3Handle) == HAL_OK) printf("spi3 init ok\n");
    else printf("spi3 init fail\n");
}

inline void init_button() {
    GPIO_InitTypeDef GPIO_Button_Struct;
    __GPIOC_CLK_ENABLE();
    GPIO_Button_Struct.Pin = GPIO_PIN_13;
    GPIO_Button_Struct.Pull = GPIO_NOPULL;
    GPIO_Button_Struct.Mode = GPIO_MODE_IT_FALLING;
    HAL_GPIO_Init(GPIOC, &GPIO_Button_Struct);
    HAL_NVIC_SetPriority((IRQn_Type)(EXTI15_10_IRQn), 0, 0);
    HAL_NVIC_EnableIRQ((IRQn_Type)(EXTI15_10_IRQn));
}

void SPI1_IRQHandler(void)
{
    // printf("SPI1 interupt\n");
    HAL_SPI_IRQHandler(&Spi1Handle);
}

void SPI2_IRQHandler(void)
{
    // printf("SPI2 interupt\n");
    HAL_SPI_IRQHandler(&Spi2Handle);
}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi) {
    printf("rx interupt...{%s}\n", res1);
}

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi) {
    printf("tx interupt...{%s}\n", src1);
}


void DMA1_Stream3_IRQHandler(void)
{
  HAL_DMA_IRQHandler(Spi2Handle.hdmarx);
}

void DMA1_Stream4_IRQHandler(void)
{
  HAL_DMA_IRQHandler(Spi2Handle.hdmatx);
}

void DMA1_Stream2_IRQHandler(void)
{
  HAL_DMA_IRQHandler(Spi3Handle.hdmarx);
}

void DMA1_Stream5_IRQHandler(void)
{
  HAL_DMA_IRQHandler(Spi3Handle.hdmatx);
}

void DMA2_Stream2_IRQHandler(void)
{
  HAL_DMA_IRQHandler(Spi1Handle.hdmarx);
  printf("interupt rx\n");
}

void DMA2_Stream3_IRQHandler(void)
{
  HAL_DMA_IRQHandler(Spi1Handle.hdmatx);
  printf("interupt tx\n");
}


void EXTI15_10_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == GPIO_PIN_13) {
        printf("Send data\n");
        start_spi();
    }
}

static void TransferComplete(DMA_HandleTypeDef *DmaHandle) {
    printf("Complete.\n");
    printf("Res: %s\n", res1);
}
