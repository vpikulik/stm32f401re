
#include <stdio.h>
#include <stdlib.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "main.h"

#include "stm32f4xx_hal.h"

#include "lwip/err.h"
#include "lwip/ip_addr.h"
#include "netif/etharp.h"


struct netif gnetif;
SemaphoreHandle_t enc_int_semaphore;
static SPI_HandleTypeDef Spi1Handle;
static void network_config(void);

extern err_t ethernetif_init(struct netif *netif);
extern void  ethernetif_input(struct netif *netif);

static void enc_get_packet_task(void *pvParameters);

void spi_enc28j60_init();
void enable_enc28j60();
void disable_enc28j60();


int main(void) {
    HAL_Init();
    HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);


    enc_int_semaphore = xSemaphoreCreateBinary();


    spi_enc28j60_init();
    lwip_init();
    network_config();

    xTaskCreate(enc_get_packet_task, "get_packet", configMINIMAL_STACK_SIZE,
                NULL, tskIDLE_PRIORITY, ( TaskHandle_t * ) NULL);

    vTaskStartScheduler();
    for(;;);
}


void spi_enc28j60_init() {
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_SPI1_CLK_ENABLE();

    GPIO_InitTypeDef GPIO_SPI1;

    // SPI1 pins
    GPIO_SPI1.Pin       = GPIO_PIN_5;
    GPIO_SPI1.Mode      = GPIO_MODE_AF_PP;
    GPIO_SPI1.Pull      = GPIO_PULLUP;
    GPIO_SPI1.Speed     = GPIO_SPEED_FAST;
    GPIO_SPI1.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_SPI1);
    GPIO_SPI1.Pin = GPIO_PIN_6;
    GPIO_SPI1.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_SPI1);
    GPIO_SPI1.Pin = GPIO_PIN_7;
    GPIO_SPI1.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_SPI1);

    // SPI CS pin
    GPIO_SPI1.Pin       = GPIO_PIN_4;
    GPIO_SPI1.Speed     = GPIO_SPEED_FAST;
    GPIO_SPI1.Mode      = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(GPIOA, &GPIO_SPI1);

    disable_enc28j60();
    // init SPI1
    Spi1Handle.Instance               = SPI1;
    Spi1Handle.Init.Direction         = SPI_DIRECTION_2LINES;
    Spi1Handle.Init.CLKPhase          = SPI_PHASE_1EDGE;
    Spi1Handle.Init.CLKPolarity       = SPI_POLARITY_LOW;
    Spi1Handle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
    Spi1Handle.Init.CRCPolynomial     = 7;
    Spi1Handle.Init.DataSize          = SPI_DATASIZE_8BIT;
    Spi1Handle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
    Spi1Handle.Init.NSS               = SPI_NSS_SOFT;
    Spi1Handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256; // TODO: change
    Spi1Handle.Init.TIMode            = SPI_TIMODE_DISABLE;
    Spi1Handle.Init.Mode = SPI_MODE_MASTER;
    if (HAL_SPI_Init(&Spi1Handle) != HAL_OK) printf("spi failed!\n");
  
    //reset ENC28J60
    GPIO_SPI1.Pin       = GPIO_PIN_8;
    GPIO_SPI1.Speed     = GPIO_SPEED_FAST;
    GPIO_SPI1.Mode      = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(GPIOA, &GPIO_SPI1);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);

    GPIO_SPI1.Pin       = GPIO_PIN_9;
    GPIO_SPI1.Mode      = GPIO_MODE_IT_FALLING;
    GPIO_SPI1.Pull      = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_SPI1);
    HAL_NVIC_SetPriority((IRQn_Type)(EXTI9_5_IRQn), 90, 0);
    HAL_NVIC_EnableIRQ((IRQn_Type)(EXTI9_5_IRQn));
}


void enable_enc28j60() {
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
}

// Disable ENC28J60 then enable interupts
void disable_enc28j60() {
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
}

uint8_t spi_enc28j60_rxtx(uint8_t data) {
    uint8_t response;
    HAL_SPI_TransmitReceive(
        &Spi1Handle,
        (uint8_t*)&data,
        (uint8_t*)&response,
        1,
        1000
    );
    return response;
}


static void network_config(void)
{
    ip_addr_t ipaddr;
    ip_addr_t netmask;
    ip_addr_t gw;

    IP4_ADDR(&ipaddr, IP_ADDR0, IP_ADDR1, IP_ADDR2, IP_ADDR3);
    IP4_ADDR(&netmask, NETMASK_ADDR0, NETMASK_ADDR1 , NETMASK_ADDR2, NETMASK_ADDR3);
    IP4_ADDR(&gw, GW_ADDR0, GW_ADDR1, GW_ADDR2, GW_ADDR3);

    /* add the network interface */    
    netif_add(&gnetif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init, &ethernet_input);

    /*  Registers the default network interface */
    netif_set_default(&gnetif);
    netif_set_up(&gnetif);

    // if (netif_is_link_up(&gnetif)) {
    //     /* When the netif is fully configured this function must be called */
    //     netif_set_up(&gnetif);
    // } else {
    //     /* When the netif link is down this function must be called */
    //     netif_set_down(&gnetif);
    // }

    /* Set the link callback function, this function is called on change of link status*/
    // netif_set_link_callback(&gnetif, ethernetif_update_config);
}

void enc_get_packet_task(void *pvParameters) {

    for( ;; )
    {
        if(xSemaphoreTake(enc_int_semaphore, portMAX_DELAY) == pdTRUE) {
            ethernetif_input(&gnetif);
        }
    }
}

void EXTI9_5_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_9);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    long lHigherPriorityTaskWoken = pdFALSE;
    if (GPIO_Pin == GPIO_PIN_9) {
        printf("Int\n");
        xSemaphoreGiveFromISR(enc_int_semaphore, &lHigherPriorityTaskWoken);
    }
    portEND_SWITCHING_ISR(lHigherPriorityTaskWoken);
}

// void SysTick_Handler(void)
// {
//     HAL_IncTick();
// }
