
#define ETH_HWADDR_0 0x00
#define ETH_HWADDR_1 0x11
#define ETH_HWADDR_2 0x22
#define ETH_HWADDR_3 0x33
#define ETH_HWADDR_4 0x44
#define ETH_HWADDR_5 0x55

#define ETHERNET_MTU 1500

#define DEST_IP_ADDR0   192
#define DEST_IP_ADDR1   168
#define DEST_IP_ADDR2   0
#define DEST_IP_ADDR3   11
#define DEST_PORT       7
/*Static IP ADDRESS: IP_ADDR0.IP_ADDR1.IP_ADDR2.IP_ADDR3 */
#define IP_ADDR0   192
#define IP_ADDR1   168
#define IP_ADDR2   0
#define IP_ADDR3   10
/*NETMASK*/
#define NETMASK_ADDR0   255
#define NETMASK_ADDR1   255
#define NETMASK_ADDR2   255
#define NETMASK_ADDR3   0
/*Gateway Address*/
#define GW_ADDR0   192
#define GW_ADDR1   168
#define GW_ADDR2   0
#define GW_ADDR3   1