#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"

#define SCLK_PIN GPIO_PIN_4
#define RCLK_PIN GPIO_PIN_5
#define DIO_PIN GPIO_PIN_3

TIM_HandleTypeDef Tim4_config;
uint8_t active_digit = 0;
uint8_t input[4] = {2, 0, 1, 5};

void init_tube_pins();
void init_timer();
void display_1led(uint8_t digit, uint8_t input[4]);
void display_led(unsigned char elem);


unsigned char digit_map[] =  {
 // 0    1    2    3    4    5    6    7    8    9    A    b    C    d    E    F    -
    0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x82,0xF8,0x80,0x90,0x8C,0xBF,0xC6,0xA1,0x86,0xFF,0xbf
};


int main(void) {

    HAL_Init();
    init_tube_pins();
    init_timer();

    while(1) {
    };
}

void init_tube_pins(void) {
    __GPIOB_CLK_ENABLE();
    GPIO_InitTypeDef  GPIO_init;
    GPIO_init.Pin = SCLK_PIN | RCLK_PIN | DIO_PIN;
    GPIO_init.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_init.Speed = GPIO_SPEED_FAST;
    HAL_GPIO_Init(GPIOB, &GPIO_init);
    HAL_GPIO_WritePin(GPIOB, SCLK_PIN, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB, RCLK_PIN, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB, DIO_PIN, GPIO_PIN_RESET);
}

void init_timer() {
    __HAL_RCC_TIM4_CLK_ENABLE();

    RCC_ClkInitTypeDef sClokConfig;
    uint32_t pFLatency;
    uint32_t uwTimclock, uwAPB1Prescaler = 0;

    HAL_RCC_GetClockConfig(&sClokConfig, &pFLatency);
    uwAPB1Prescaler = sClokConfig.APB1CLKDivider;
    if (uwAPB1Prescaler == 0) {
        uwTimclock = HAL_RCC_GetPCLK1Freq();
    }
    else {
        uwTimclock = 2*HAL_RCC_GetPCLK1Freq();
    }

    Tim4_config.Instance = TIM4;
    Tim4_config.Init.Period = 24;
    Tim4_config.Init.Prescaler = (uint32_t) ((uwTimclock / 10000) - 1); // 100Mhz
    Tim4_config.Init.ClockDivision = 0;
    Tim4_config.Init.CounterMode = TIM_COUNTERMODE_UP;
    // Tim4_config.Init.RepetitionCounter = 0;

    HAL_TIM_Base_Init(&Tim4_config);

    HAL_NVIC_SetPriority(TIM4_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ((IRQn_Type)(TIM4_IRQn));

    HAL_TIM_Base_Start_IT(&Tim4_config);
}

void display_1led(uint8_t digit, uint8_t input[4]) {
    display_led(digit_map[input[digit]]);
    display_led(1 << (3-digit));
    HAL_GPIO_WritePin(GPIOB, RCLK_PIN, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB, RCLK_PIN, GPIO_PIN_SET);
}

void display_led(uint8_t elem) {
    uint8_t i;
    for(i = 8; i >= 1; i--)
    {
        if (elem & 0x80) {
            HAL_GPIO_WritePin(GPIOB, DIO_PIN, GPIO_PIN_SET);
        } else {
            HAL_GPIO_WritePin(GPIOB, DIO_PIN, GPIO_PIN_RESET);
        }
        elem <<= 1;
        HAL_GPIO_WritePin(GPIOB, SCLK_PIN, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(GPIOB, SCLK_PIN, GPIO_PIN_SET);
    }
}

void TIM4_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&Tim4_config);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    // printf("Timer interrupt\n");
    if (active_digit == 4) {
        active_digit = 0;
    }
    display_1led(active_digit, input);
    active_digit ++;
}
