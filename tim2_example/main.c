

/* Includes ------------------------------------------------------------------*/
#include  <stdio.h>

#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile uint16_t led_status = 0;
TIM_HandleTypeDef keyboard_tim_config;

/* Private function prototypes -----------------------------------------------*/
void init_led();
void toggle_led();
TIM_HandleTypeDef init_timer();

/* Private functions ---------------------------------------------------------*/


int main(void)
{
    uint32_t counter_tmp = 0;
   
    HAL_Init();

    init_led();
    init_timer();
    printf("Timer started!\n");

    while (1) {
        // counter_tmp = timer.Instance->CNT;
    }
}

void init_led() {
    GPIO_InitTypeDef  GPIO_Led_Struct;
    __GPIOA_CLK_ENABLE();
    GPIO_Led_Struct.Pin = GPIO_PIN_5;
    GPIO_Led_Struct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_Led_Struct.Speed = GPIO_SPEED_FAST;
    HAL_GPIO_Init(GPIOA, &GPIO_Led_Struct);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
}

void toggle_led() {
    if (led_status == 1) {
        led_status = 0;
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
    } else {
        led_status = 1;
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
    }
}

TIM_HandleTypeDef init_timer() {
    __HAL_RCC_TIM3_CLK_ENABLE();

    RCC_ClkInitTypeDef sClokConfig;
    uint32_t pFLatency;
    uint32_t uwTimclock, uwAPB1Prescaler = 0;

    HAL_RCC_GetClockConfig(&sClokConfig, &pFLatency);
    uwAPB1Prescaler = sClokConfig.APB1CLKDivider;
    if (uwAPB1Prescaler == 0) {
        uwTimclock = HAL_RCC_GetPCLK1Freq();
    }
    else {
        uwTimclock = 2*HAL_RCC_GetPCLK1Freq();
    }


    keyboard_tim_config.Instance = TIM3;
    keyboard_tim_config.Init.Period = 999;
    keyboard_tim_config.Init.Prescaler = (uint32_t) ((uwTimclock / 1000) - 1); // 1khz
    keyboard_tim_config.Init.ClockDivision = 0;
    keyboard_tim_config.Init.CounterMode = TIM_COUNTERMODE_UP;
    // keyboard_tim_config.Init.RepetitionCounter = 0;

    HAL_TIM_Base_Init(&keyboard_tim_config);

    HAL_NVIC_SetPriority(TIM3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ((IRQn_Type)(TIM3_IRQn));

    HAL_TIM_Base_Start_IT(&keyboard_tim_config);
}

void TIM3_IRQHandler(void)
{
    // printf("Iteruption!\n");
    HAL_TIM_IRQHandler(&keyboard_tim_config);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    // printf("Timer interuption!\n");
    toggle_led();
}
