

/* Includes ------------------------------------------------------------------*/
#include  <stdio.h>

#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"
#include "stm32f4xx_hal_dma.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

#define DMA_STREAM               DMA2_Stream0
#define DMA_CHANNEL              DMA_CHANNEL_0 
#define DMA_STREAM_IRQ           DMA2_Stream0_IRQn
#define DMA_STREAM_IRQHANDLER    DMA2_Stream0_IRQHandler

#define BUFFER_SIZE              32
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

char src[15] = "Hello from DMA!";
char res[15];

DMA_HandleTypeDef DmaHandle;

/* Private function prototypes -----------------------------------------------*/
inline void init_button();
inline void init_dma();
inline start_dma(DMA_HandleTypeDef *DmaHandle);

static void TransferComplete(DMA_HandleTypeDef *DmaHandle);
static void TransferError(DMA_HandleTypeDef *DmaHandle);

/* Private functions ---------------------------------------------------------*/


int main(void)
{
   
    HAL_Init();

    init_button();
    init_dma();
    printf("Init.\n");

    while (1) {
    }
}

inline void init_dma() {
    __HAL_RCC_DMA2_CLK_ENABLE();

    DmaHandle.Init.Channel = DMA_CHANNEL;                     /* DMA_CHANNEL_0                    */                     
    DmaHandle.Init.Direction = DMA_MEMORY_TO_MEMORY;          /* M2M transfer mode                */           
    DmaHandle.Init.PeriphInc = DMA_PINC_ENABLE;               /* Peripheral increment mode Enable */                 
    DmaHandle.Init.MemInc = DMA_MINC_ENABLE;                  /* Memory increment mode Enable     */                   
    DmaHandle.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD; /* Peripheral data alignment : Word */    
    DmaHandle.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;    /* memory data alignment : Word     */     
    DmaHandle.Init.Mode = DMA_NORMAL;                         /* Normal DMA mode                  */  
    DmaHandle.Init.Priority = DMA_PRIORITY_HIGH;              /* priority level : high            */  
    DmaHandle.Init.FIFOMode = DMA_FIFOMODE_DISABLE;           /* FIFO mode disabled               */        
    DmaHandle.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;  
    DmaHandle.Init.MemBurst = DMA_MBURST_SINGLE;              /* Memory burst                     */  
    DmaHandle.Init.PeriphBurst = DMA_PBURST_SINGLE;           /* Peripheral burst                 */

    DmaHandle.Instance = DMA_STREAM;

    DmaHandle.XferCpltCallback  = TransferComplete;
    DmaHandle.XferErrorCallback = TransferError;

    HAL_DMA_Init(&DmaHandle);

    HAL_NVIC_SetPriority(DMA_STREAM_IRQ, 0, 0);
    HAL_NVIC_EnableIRQ(DMA_STREAM_IRQ);
}

inline start_dma(DMA_HandleTypeDef *DmaHandle) {
    HAL_DMA_Start_IT(DmaHandle, (uint32_t)&src, (uint32_t)&res, 15);
}

inline void init_button() {
    GPIO_InitTypeDef GPIO_Button_Struct;
    __GPIOC_CLK_ENABLE();
    GPIO_Button_Struct.Pin = GPIO_PIN_13;
    GPIO_Button_Struct.Pull = GPIO_NOPULL;
    GPIO_Button_Struct.Mode = GPIO_MODE_IT_FALLING;
    HAL_GPIO_Init(GPIOC, &GPIO_Button_Struct);
    HAL_NVIC_SetPriority((IRQn_Type)(EXTI15_10_IRQn), 0, 0x00);
    HAL_NVIC_EnableIRQ((IRQn_Type)(EXTI15_10_IRQn));
}

static void TransferComplete(DMA_HandleTypeDef *DmaHandle) {
    printf("Complete.\n");
    printf("Src: %s\n", src);
    printf("Res: %s\n", res);
}

static void TransferError(DMA_HandleTypeDef *DmaHandle) {
    printf("Error.\n");
}

void DMA_STREAM_IRQHANDLER(void)
{
    /* Check the interrupt and clear flag */
    HAL_DMA_IRQHandler(&DmaHandle);  
}

void EXTI15_10_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == GPIO_PIN_13) {
        start_dma(&DmaHandle);
    }
}
