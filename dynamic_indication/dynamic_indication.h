
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"


typedef struct {
    GPIO_TypeDef* port;
    uint16_t pin;
} IndicatorPin;

typedef struct {
    IndicatorPin a;
    IndicatorPin b;
    IndicatorPin c;
    IndicatorPin d;
    IndicatorPin e;
    IndicatorPin f;
    IndicatorPin g;
    IndicatorPin h;
} IndicatorConnector;

typedef struct {
    IndicatorConnector* connector;
    IndicatorPin switcher;
    uint8_t value;
} Indicator;

typedef struct {
    uint8_t count;
    Indicator** indicators;
    uint8_t _current;
} IndicatorSet;

inline Indicator indicator_create(IndicatorConnector* connector,
                                  IndicatorPin switcher, uint8_t input,
                                  uint8_t point);
inline indicator_update(Indicator* indicator, uint8_t input, uint8_t point);
inline IndicatorSet indicator_create_set(uint8_t count,
                                         Indicator** indicators);
void indicator_display_one(Indicator* indicator);
void indicator_display(IndicatorSet* set);
