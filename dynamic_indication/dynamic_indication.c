
#include "dynamic_indication.h"


static uint8_t _convert(uint8_t value) {
    uint8_t res;
    switch (value) {
        case 0x0:
            res = (uint8_t) 0b11111100;
            break;
        case 0x1:
            res = (uint8_t) 0b01100000;
            break;
        case 0x2:
            res = (uint8_t) 0b11011010;
            break;
        case 0x3:
            res = (uint8_t) 0b11110010;
            break;
        case 0x4:
            res = (uint8_t) 0b01100110;
            break;
        case 0x5:
            res = (uint8_t) 0b10110110;
            break;
        case 0x6:
            res = (uint8_t) 0b10111110;
            break;
        case 0x7:
            res = (uint8_t) 0b11100000;
            break;
        case 0x8:
            res = (uint8_t) 0b11111110;
            break;
        case 0x9:
            res = (uint8_t) 0b11110110;
            break;
        case 0xa:
            res = (uint8_t) 0b11101110;
            break;
        case 0xb:
            res = (uint8_t) 0b00111110;
            break;
        case 0xc:
            res = (uint8_t) 0b10011100;
            break;
        case 0xd:
            res = (uint8_t) 0b01111010;
            break;
        case 0xe:
            res = (uint8_t) 0b10011110;
            break;
        case 0xf:
            res = (uint8_t) 0b10001110;
            break;
        return res;
    }
}


inline Indicator indicator_create(IndicatorConnector* connector,
                                  IndicatorPin switcher, uint8_t input,
                                  uint8_t point) {
    Indicator indicator;
    indicator.connector = connector;
    indicator.switcher = switcher;
    indicator_update(&indicator, input, point);
    return indicator;
}

inline indicator_update(Indicator* indicator, uint8_t input, uint8_t point) {
    uint8_t value = _convert(input);
    if (point) {
        value |= 0b1;
    }
    indicator->value = value;
}

inline IndicatorSet indicator_create_set(uint8_t count,
                                         Indicator** indicators) {
    IndicatorSet set;
    set.count = count;
    set.indicators = indicators;
    set._current = 0;
    return set;
}

void indicator_display_one(Indicator *indicator) {
    IndicatorConnector* connector = indicator->connector;
    uint8_t value = indicator->value;
    HAL_GPIO_WritePin(connector->a.port, connector->a.pin, value & 1<<7);
    HAL_GPIO_WritePin(connector->b.port, connector->b.pin, value & 1<<6);
    HAL_GPIO_WritePin(connector->c.port, connector->c.pin, value & 1<<5);
    HAL_GPIO_WritePin(connector->d.port, connector->d.pin, value & 1<<4);
    HAL_GPIO_WritePin(connector->e.port, connector->e.pin, value & 1<<3);
    HAL_GPIO_WritePin(connector->f.port, connector->f.pin, value & 1<<2);
    HAL_GPIO_WritePin(connector->g.port, connector->g.pin, value & 1<<1);
    HAL_GPIO_WritePin(connector->h.port, connector->h.pin, value & 1<<0);
    HAL_GPIO_WritePin(indicator->switcher.port,
                      indicator->switcher.pin, GPIO_PIN_RESET);
}

void indicator_display(IndicatorSet* set) {
    uint8_t i;
    for (i=0; i<set->count; i++) {
        if (i == set->_current) {
            continue;
        }
        Indicator* indicator = set->indicators[i];
        // HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_SET);
        HAL_GPIO_WritePin(indicator->switcher.port, indicator->switcher.pin,
                          GPIO_PIN_SET);
    }
    Indicator* indicator = set->indicators[set->_current];
    indicator_display_one(indicator);

    set->_current ++;
    if (set->_current == set->count) {
        set->_current = 0;
    }
}
