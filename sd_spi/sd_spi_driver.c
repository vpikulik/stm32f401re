
#include "sd_spi_driver.h"


#define BLOCK_SIZE  512


DSTATUS SD_SPI_initialize (BYTE);
DSTATUS SD_SPI_status (BYTE);
DRESULT SD_SPI_read (BYTE, BYTE*, DWORD, UINT);
#if _USE_WRITE == 1
    DRESULT SD_SPI_write (BYTE, const BYTE*, DWORD, UINT);
#endif /* _USE_WRITE == 1 */
#if _USE_IOCTL == 1
    DRESULT SD_SPI_ioctl (BYTE, BYTE, void*);
#endif  /* _USE_IOCTL == 1 */


Diskio_drvTypeDef SD_SPI_Driver =
{
    SD_SPI_initialize,
    SD_SPI_status,
    SD_SPI_read,
#if  _USE_WRITE == 1
    SD_SPI_write,
#endif
#if  _USE_IOCTL == 1
    SD_SPI_ioctl,
#endif
};

hwif hw;


DSTATUS SD_SPI_initialize (BYTE lun)
{
    if (hwif_init(&hw) == 0)
        return 0;
    return STA_NOINIT;
}


DSTATUS SD_SPI_status(BYTE lun)
{
    if (hw.initialized)
        return 0;
    return STA_NOINIT;
}


DRESULT SD_SPI_read(BYTE lun, BYTE *buff, DWORD sector, UINT count)
{
    int i;
    for (i=0; i<count; i++)
        if (sd_read(&hw, sector+i, buff+BLOCK_SIZE*i) != 0)
            return RES_ERROR;
    return RES_OK;
}


#if _USE_WRITE == 1
DRESULT SD_SPI_write(BYTE lun, const BYTE *buff, DWORD sector, UINT count)
{
    int i;
    for (i=0; i<count; i++)
        if (sd_write(&hw, sector+i, buff+BLOCK_SIZE*i) != 0)
            return RES_ERROR;
    return RES_OK;
}
#endif


#if _USE_IOCTL == 1
DRESULT SD_SPI_ioctl(BYTE lun, BYTE cmd, void *buff)
{
    switch (cmd) {
    case CTRL_SYNC:
        return RES_OK;
    case GET_SECTOR_SIZE:
        *(WORD*)buff = BLOCK_SIZE;
        return RES_OK;
    case GET_SECTOR_COUNT:
        *(DWORD*)buff = hw.sectors;
        return RES_OK;
    case GET_BLOCK_SIZE:
        *(DWORD*)buff = hw.erase_sectors;
        return RES_OK;
    }
    return RES_PARERR;
}
#endif
