

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include "stm32f4xx_hal.h"

#include "ff_gen_drv.h"
#include "sd_spi_driver.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

FATFS FatFs;

int main(void)
{
    FIL fil;       /* File object */
    char line[82]; /* Line buffer */
    char message[] = "Hello world!!!";
    FRESULT fr;    /* FatFs return code */
    UINT res_len;

    if(FATFS_LinkDriver(&SD_SPI_Driver, 0) == 0)
    {
        printf("Read\n");
        f_mount(&FatFs, "", 0);
        fr = f_open(&fil, "message.txt", FA_READ);
        while (f_gets(line, sizeof line, &fil)) {
            printf(line);
        }
        f_close(&fil);
        f_mount(NULL, "", 0);

        printf("Write\n");
        f_mount(&FatFs, "", 0);
        fr = f_open(&fil, "file.txt", FA_CREATE_ALWAYS | FA_WRITE);
        f_write(&fil, message, sizeof message, &res_len);
        printf("size: %i, %i, %s\n", sizeof message, res_len, message);
        f_close(&fil);
        f_mount(NULL, "", 0);
    }

    while (1) {

    }
}
