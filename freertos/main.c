
#include <stdio.h>
#include <stdlib.h>

#include "stm32f4xx_hal.h"

#include "cmsis_os.h"


typedef struct {
    uint32_t id;
} Message;

osMessageQId main_queue;
static osMutexId mutex;


static void osTimerCallback (void const *argument);
static void SenderHandler(void const *argument);
static void Receiver1Handler(void const *argument);
static void Receiver2Handler(void const *argument);


int main(void) {

    HAL_Init();

    /* Create Timer */
    osTimerDef(MyTimer, osTimerCallback);
    osTimerId osTimer = osTimerCreate(osTimer(MyTimer), osTimerPeriodic, NULL);
    /* Start Timer */
    osTimerStart(osTimer, 200);

    osMutexDef(mutex);
    mutex = osMutexCreate(osMutex(mutex));

    osMessageQDef(MainQueue, 10, uint32_t);
    main_queue = osMessageCreate(osMessageQ(MainQueue), NULL);

    osThreadDef(Sender, SenderHandler, osPriorityAboveNormal, 0, configMINIMAL_STACK_SIZE);
    osThreadDef(Receiver1, Receiver1Handler, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
    osThreadDef(Receiver2, Receiver2Handler, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);

    /* Create Thread */
    printf("Before: %i\n", xPortGetFreeHeapSize());
    osThreadCreate(osThread(Sender), NULL);
    printf("T1: %i\n", xPortGetFreeHeapSize());
    osThreadCreate(osThread(Receiver1), NULL);
    printf("T2: %i\n", xPortGetFreeHeapSize());
    osThreadCreate(osThread(Receiver2), NULL);
    printf("T3: %i\n", xPortGetFreeHeapSize());

    /* Start scheduler */
    osKernelStart();

    /* We should never get here as control is now taken by the scheduler */
    for(;;);
}


static void osTimerCallback (void const *argument)
{
    (void) argument;

    // printf("OS timer\n");
}


static void SenderHandler(void const *argument)
{
    (void) argument;
    uint32_t counter = 0;

    while (1) {
        if (osMutexWait(mutex, 3) == osOK) {
            if(osMessagePut(main_queue, counter, 300) == osOK) {
                printf("Message with id=%i was sent\n", counter);
            } else {
                printf("Queue is blocked i=%i\n", counter);
            }
            counter++;
            osMutexRelease(mutex);
        }
        osDelay(100);
    }
}


static void Receiver1Handler(void const *argument)
{
    (void) argument;
    osEvent event;

    for(;;)
    {
        if (osMutexWait(mutex, 3) == osOK) {
            event = osMessageGet(main_queue, 200);
            if(event.status == osEventMessage) {
                printf("R1: Received message with value: %i\n", event.value.v);
            } else if (event.status == osEventTimeout) {
                printf("R1: Message timeout\n");
            }
            osMutexRelease(mutex);
        }
        osDelay(300);
    }
}

static void Receiver2Handler(void const *argument)
{
    // (void) argument;
    // osEvent event;

    for(;;)
    {
        // if (osMutexWait(mutex, 3) == osOK) {
            // event = osMessageGet(main_queue, 200);
            // if(event.status == osEventMessage) {
            //     printf("R2: Received message with value: %i\n", event.value.v);
            // } else if (event.status == osEventTimeout) {
            //     printf("R2: Message timeout\n");
            // }
            printf("Receiver2: %i\n", xPortGetFreeHeapSize());
            // osMutexRelease(mutex);
        // }
        osDelay(500);
    }
}


void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Error\n");
    while (1)
    {
    }
}


void SysTick_Handler(void)
{
    osSystickHandler();
}