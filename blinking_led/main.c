#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"

#define MIN_FREQ 100
#define MAX_FREQ 1000
#define STEP_FREQ 100

void toggle_led();
void change_frequency();

volatile uint32_t freq = MIN_FREQ;
volatile uint16_t led_status = 0;

int main(void) {

    HAL_Init();

    HAL_SYSTICK_Config(SystemCoreClock/1000);

    GPIO_InitTypeDef  GPIO_Led_Struct;
    __GPIOA_CLK_ENABLE();
    GPIO_Led_Struct.Pin = GPIO_PIN_5;
    GPIO_Led_Struct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_Led_Struct.Speed = GPIO_SPEED_FAST;
    HAL_GPIO_Init(GPIOA, &GPIO_Led_Struct);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);

    GPIO_InitTypeDef GPIO_Button_Struct;
    __GPIOC_CLK_ENABLE();
    GPIO_Button_Struct.Pin = GPIO_PIN_13;
    GPIO_Button_Struct.Pull = GPIO_NOPULL;
    GPIO_Button_Struct.Mode = GPIO_MODE_IT_FALLING; 
    HAL_GPIO_Init(GPIOC, &GPIO_Button_Struct);
    HAL_NVIC_SetPriority((IRQn_Type)(EXTI15_10_IRQn), 0x0F, 0x00);
    HAL_NVIC_EnableIRQ((IRQn_Type)(EXTI15_10_IRQn));

    while(1) {
        toggle_led();
        HAL_Delay(freq);
    };
}

void toggle_led() {
    if (led_status) {
        led_status = 0;
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
    } else {
        led_status = 1;
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
    }
}

void change_frequency() {
    if (freq < MAX_FREQ) {
        freq += STEP_FREQ;
    } else {
        freq = MIN_FREQ;
    }
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if(GPIO_Pin == GPIO_PIN_13)
    {
        change_frequency();
    }
}

void HAL_SYSTICK_Callback(void)
{
  HAL_IncTick();
}
