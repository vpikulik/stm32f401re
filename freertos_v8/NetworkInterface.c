
/* Standard includes. */
#include <stdint.h>
#include <stdio.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

// /* Hardware abstraction. */
// #include "FreeRTOS_IO.h"

/* FreeRTOS+UDP includes. */
#include "FreeRTOS_UDP_IP.h"
#include "FreeRTOS_IP_Private.h"
#include "FreeRTOS_Sockets.h"
#include "NetworkBufferManagement.h"

// /* Driver includes. */
// #include "lpc17xx_emac.h"
// #include "lpc17xx_pinsel.h"

/* Demo includes. */
#include "NetworkInterface.h"

static void EthCheckInputTask( void *pvParameters );
static xSemaphoreHandle xEMACRxEventSemaphore = NULL;
xSemaphoreHandle EtnIntSignalSemaphore = NULL;
extern uint8_t ucMACAddress[6];

BaseType_t xNetworkInterfaceInitialise( void )
{
    enc28j60Init(ucMACAddress);

    if (EtnIntSignalSemaphore == NULL) {
        EtnIntSignalSemaphore = xSemaphoreCreateCounting(100, 0);
        printf("Created\n");
    }
    configASSERT(EtnIntSignalSemaphore);

    if (xEMACRxEventSemaphore == NULL) {
        vSemaphoreCreateBinary(xEMACRxEventSemaphore);
    }
    configASSERT(xEMACRxEventSemaphore);

    xTaskCreate(EthCheckInputTask, "EthCheckInputTask", 1024, NULL,
                tskIDLE_PRIORITY, ( TaskHandle_t * ) NULL);

    return pdPASS; //pdFAIL
}
/*-----------------------------------------------------------*/

BaseType_t xNetworkInterfaceOutput( xNetworkBufferDescriptor_t * const pxNetworkBuffer )
{
    uint32_t sz = pxNetworkBuffer->xDataLength;
    xSemaphoreTake( xEMACRxEventSemaphore, portMAX_DELAY );
    enc28j60PacketSend(
        pxNetworkBuffer->xDataLength,
        pxNetworkBuffer->pucEthernetBuffer
    );
    vNetworkBufferRelease( pxNetworkBuffer );
    xSemaphoreGive( xEMACRxEventSemaphore);
    printf("Eth Output. Length %i\n", sz);
    return pdPASS;
}
/*-----------------------------------------------------------*/

#define TEMP_BUFFER_SIZE 512

/* The deferred interrupt handler is a standard RTOS task. */
static void EthCheckInputTask( void *pvParameters )
{
    xNetworkBufferDescriptor_t *pxNetworkBuffer;
    size_t xBytesReceived;
    extern QueueHandle_t xNetworkEventQueue;
    xIPStackEvent_t xRxEvent;

    configASSERT(EtnIntSignalSemaphore);
    configASSERT(xEMACRxEventSemaphore);

    for( ;; )
    {
        /* Wait fot enc28j60 interupt signal */
        xSemaphoreTake(EtnIntSignalSemaphore, portMAX_DELAY);

        /* Wait for the Ethernet MAC interrupt to indicate that another packet
        has been received.  It is assumed xEMACRxEventSemaphore is a counting
        semaphore (to count the Rx events) and that the semaphore has already
        been created. */
        xSemaphoreTake( xEMACRxEventSemaphore, portMAX_DELAY );

        /* See how much data was received.  Here it is assumed ReceiveSize() is
        a peripheral driver function that returns the number of bytes in the
        received Ethernet frame. */
        uint8_t *tmp_buf = (uint8_t*) pvPortMalloc(TEMP_BUFFER_SIZE);
        xBytesReceived = enc28j60PacketReceive(
            TEMP_BUFFER_SIZE,
            tmp_buf
        );
        printf("EthCheck. Received %i bytes\n", xBytesReceived);
        xSemaphoreGive(xEMACRxEventSemaphore);

        if(xBytesReceived > 0)
        {
            /* Allocate a network buffer descriptor that references an Ethernet
            buffer large enough to hold the received data. */
            pxNetworkBuffer = pxNetworkBufferGet( xBytesReceived, 0 );

            if( pxNetworkBuffer != NULL )
            {
                /* pxNetworkBuffer->pucEthernetBuffer now points to an Ethernet
                buffer large enough to hold the received data.  Copy the
                received data into pcNetworkBuffer->pucEthernetBuffer.  Here it
                is assumed ReceiveData() is a peripheral driver function that
                copies the received data into a buffer passed in as the function's
                parameter. */
                memcpy(pxNetworkBuffer->pucEthernetBuffer, tmp_buf, xBytesReceived);

                /* See if the data contained in the received Ethernet frame needs
                to be processed. */
                if( eConsiderFrameForProcessing( pxNetworkBuffer->pucEthernetBuffer )
                                                                      == eProcessBuffer )
                {
                    /* The event about to be sent to the IP stack is an Rx event. */
                    xRxEvent.eEventType = eEthernetRxEvent;

                    /* pvData is used to point to the network buffer descriptor that
                    references the received data. */
                    xRxEvent.pvData = ( void * ) pxNetworkBuffer;

                    /* Send the data to the IP stack. */
                    if( xQueueSendToBack( xNetworkEventQueue, &xRxEvent, 0 ) == pdFALSE )
                    {
                        /* The buffer could not be sent to the IP task so the buffer
                        must be released. */
                        vNetworkBufferRelease( pxNetworkBuffer );

                        /* Make a call to the standard trace macro to log the
                        occurrence. */
                        iptraceETHERNET_RX_EVENT_LOST();
                    }
                    else
                    {
                        /* The message was successfully sent to the IP stack.  Call
                        the standard trace macro to log the occurrence. */
                        iptraceNETWORK_INTERFACE_RECEIVE();
                    }
                }
                else
                {
                    /* The Ethernet frame can be dropped, but the Ethernet buffer
                    must be released. */
                    vNetworkBufferRelease( pxNetworkBuffer );
                }
            }
            else
            {
                /* The event was lost because a network buffer was not available.
                Call the standard trace macro to log the occurrence. */
                iptraceETHERNET_RX_EVENT_LOST();
            }
        }
        else {
            xSemaphoreGive(xEMACRxEventSemaphore);
        }

        vPortFree(tmp_buf);
        vTaskDelay(10);
    }
}
