
#include <stdio.h>
#include <stdlib.h>

#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "FreeRTOS_UDP_IP.h"


void vTask1(void *pvParameters);
void vTask2(void *pvParameters);
static void PingTask( void *pvParameters );

void spi_enc28j60_init();
void enable_enc28j60();
void disable_enc28j60();


/* Define the network addressing.  These parameters will be used if either
ipconfigUDE_DHCP is 0 or if ipconfigUSE_DHCP is 1 but DHCP auto configuration
failed. */
static const uint8_t ucIPAddress[ 4 ] = { 192, 168, 0, 10 };
static const uint8_t ucNetMask[ 4 ] = { 255, 255, 255, 0 };
static const uint8_t ucGatewayAddress[ 4 ] = { 192, 168, 0, 1 };

/* The following is the address of an OpenDNS server. */
static const uint8_t ucDNSServerAddress[ 4 ] = { 208, 67, 222, 222 };

/* The MAC address array is not declared const as the MAC address will
normally be read from an EEPROM and not hard coded (in real deployed
applications).*/
uint8_t ucMACAddress[ 6 ] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55 };

extern xSemaphoreHandle EtnIntSignalSemaphore;
static SPI_HandleTypeDef Spi1Handle;


int main(void) {

    HAL_Init();
    HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

    spi_enc28j60_init();

    // xTaskCreate(vTask1, "Task1", configMINIMAL_STACK_SIZE, NULL,
    //             tskIDLE_PRIORITY, ( TaskHandle_t * ) NULL);
    // xTaskCreate(vTask2, "Task2", configMINIMAL_STACK_SIZE, NULL,
    //             tskIDLE_PRIORITY, ( TaskHandle_t * ) NULL);

    /* Initialise the embedded Ethernet interface.  The tasks that use the
    network are created in the vApplicationIPNetworkEventHook() hook function
    below.  The hook function is called when the network connects. */
    FreeRTOS_IPInit( ucIPAddress,
                     ucNetMask,
                     ucGatewayAddress,
                     ucDNSServerAddress,
                     ucMACAddress );

    printf("Task ready!\n");
    vTaskStartScheduler();
    printf("Error!\n");
    for(;;);
}


void vTask1(void *pvParameters)
{
    for( ;; ) {
        printf("Task1\n");
        

        vTaskDelay(100);
    }
    vTaskDelete( NULL );
}

void vTask2(void *pvParameters)
{
    for( ;; ) {
        printf("Task2\n");

        

        vTaskDelay(100);
    }
    vTaskDelete( NULL );
}

static void PingTask( void *pvParameters ) {
    extern QueueHandle_t xNetworkEventQueue;

    for (;;) {
        uint32_t addr;
        addr = FreeRTOS_inet_addr("192.168.0.1");
        FreeRTOS_SendPingRequest(addr, 8, 100/portTICK_PERIOD_MS);
        vTaskDelay(1000);
    }
}

void vApplicationIPNetworkEventHook( eIPCallbackEvent_t eNetworkEvent )
{
    printf("NetworkEventHook!\n");
    static BaseType_t xTasksAlreadyCreated = pdFALSE;

    /* Check this was a network up event, as opposed to a network down event. */
    if( eNetworkEvent == eNetworkUp )
    {
        /* Create the tasks that use the IP stack if they have not already been
        created. */
        if( xTasksAlreadyCreated == pdFALSE )
        {
            /*
             * For convenience, tasks that use FreeRTOS+UDP can be created here
             * to ensure they are not created before the network is usable.
             */

            xTasksAlreadyCreated = pdTRUE;
        }

        // xTaskCreate(PingTask, "PingTask", 128, NULL,
        //             tskIDLE_PRIORITY, ( TaskHandle_t * ) NULL);
    }
}

void vApplicationPingReplyHook( ePingReplyStatus_t eStatus, uint16_t usIdentifier ) {
    printf("Ping received !!!!!!!\n");
}

void spi_enc28j60_init() {
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_SPI1_CLK_ENABLE();

    GPIO_InitTypeDef GPIO_SPI1;

    // SPI1 pins
    GPIO_SPI1.Pin       = GPIO_PIN_5;
    GPIO_SPI1.Mode      = GPIO_MODE_AF_PP;
    GPIO_SPI1.Pull      = GPIO_PULLUP;
    GPIO_SPI1.Speed     = GPIO_SPEED_FAST;
    GPIO_SPI1.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_SPI1);
    GPIO_SPI1.Pin = GPIO_PIN_6;
    GPIO_SPI1.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_SPI1);
    GPIO_SPI1.Pin = GPIO_PIN_7;
    GPIO_SPI1.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_SPI1);

    // SPI CS pin
    GPIO_SPI1.Pin       = GPIO_PIN_4;
    GPIO_SPI1.Speed     = GPIO_SPEED_FAST;
    GPIO_SPI1.Mode      = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(GPIOA, &GPIO_SPI1);

    disable_enc28j60();
    // init SPI1
    Spi1Handle.Instance               = SPI1;
    Spi1Handle.Init.Direction         = SPI_DIRECTION_2LINES;
    Spi1Handle.Init.CLKPhase          = SPI_PHASE_1EDGE;
    Spi1Handle.Init.CLKPolarity       = SPI_POLARITY_LOW;
    Spi1Handle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
    Spi1Handle.Init.CRCPolynomial     = 7;
    Spi1Handle.Init.DataSize          = SPI_DATASIZE_8BIT;
    Spi1Handle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
    Spi1Handle.Init.NSS               = SPI_NSS_SOFT;
    Spi1Handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256; // TODO: change
    Spi1Handle.Init.TIMode            = SPI_TIMODE_DISABLE;
    Spi1Handle.Init.Mode = SPI_MODE_MASTER;
    if (HAL_SPI_Init(&Spi1Handle) != HAL_OK) printf("spi failed!\n");
  
    //reset ENC28J60
    GPIO_SPI1.Pin       = GPIO_PIN_8;
    GPIO_SPI1.Speed     = GPIO_SPEED_FAST;
    GPIO_SPI1.Mode      = GPIO_MODE_OUTPUT_PP;
    HAL_GPIO_Init(GPIOA, &GPIO_SPI1);
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);

    GPIO_SPI1.Pin       = GPIO_PIN_9;
    GPIO_SPI1.Mode      = GPIO_MODE_IT_FALLING;
    GPIO_SPI1.Pull      = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_SPI1);
    HAL_NVIC_SetPriority((IRQn_Type)(EXTI9_5_IRQn), 90, 0);
    HAL_NVIC_EnableIRQ((IRQn_Type)(EXTI9_5_IRQn));
}

// Enable ENC28J60 after disabling interupts
void enable_enc28j60() {
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
}

// Disable ENC28J60 then enable interupts
void disable_enc28j60() {
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
}

uint8_t spi_enc28j60_rxtx(uint8_t data) {
    uint8_t response;
    HAL_SPI_TransmitReceive(
        &Spi1Handle,
        (uint8_t*)&data,
        (uint8_t*)&response,
        1,
        1000
    );
    return response;
}

void EXTI9_5_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_9);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    long lHigherPriorityTaskWoken = pdFALSE;
    if (GPIO_Pin == GPIO_PIN_9) {
        printf("Int\n");
        configASSERT(EtnIntSignalSemaphore);
        xSemaphoreGiveFromISR(EtnIntSignalSemaphore, &lHigherPriorityTaskWoken);
    }
    portEND_SWITCHING_ISR(lHigherPriorityTaskWoken);
}
