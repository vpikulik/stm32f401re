

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"
#include "stm32f4xx_hal_spi.h"
#include "stm32f4xx_hal_tim.h"

#include "dynamic_indication.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

char res1[3];
uint8_t message_length;
uint8_t* message;

SPI_HandleTypeDef Spi1Handle;
GPIO_InitTypeDef  GPIO_InitStruct;
IndicatorConnector connector;
Indicator indicator1;
Indicator indicator2;
IndicatorSet iset;
TIM_HandleTypeDef Tim4_config;

/* Private function prototypes -----------------------------------------------*/
inline void init_button();
inline void init_indicators();
inline void init_spi1();
inline void init_timer();
inline start_spi();


/* Private functions ---------------------------------------------------------*/


int main(void)
{
   
    HAL_Init();

    init_button();
    init_indicators();
    init_spi1();
    init_timer();
    printf("Init.\n");

    start_spi();

    while (1) {
    }
}

inline start_spi() {
    message_length = 4;
    message = (uint8_t*) malloc(message_length * sizeof(uint8_t));
    HAL_SPI_Receive_IT(&Spi1Handle, message, message_length);
}

inline void init_spi1() {
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_SPI1_CLK_ENABLE();
    __HAL_RCC_DMA2_CLK_ENABLE();

    GPIO_InitStruct.Pin       = GPIO_PIN_5;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull      = GPIO_PULLUP;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = GPIO_PIN_6;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    Spi1Handle.Instance               = SPI1;
    Spi1Handle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
    Spi1Handle.Init.Direction         = SPI_DIRECTION_2LINES;
    Spi1Handle.Init.CLKPhase          = SPI_PHASE_1EDGE;
    Spi1Handle.Init.CLKPolarity       = SPI_POLARITY_HIGH;
    Spi1Handle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
    Spi1Handle.Init.CRCPolynomial     = 7;
    Spi1Handle.Init.DataSize          = SPI_DATASIZE_8BIT;
    Spi1Handle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
    Spi1Handle.Init.NSS               = SPI_NSS_SOFT;
    Spi1Handle.Init.TIMode            = SPI_TIMODE_DISABLE;
    Spi1Handle.Init.Mode = SPI_MODE_SLAVE;

    HAL_NVIC_SetPriority(SPI1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(SPI1_IRQn);

    if (HAL_SPI_Init(&Spi1Handle) == HAL_OK) printf("spi1 init ok\n");
    else printf("spi1 init fail\n");
}

inline void init_button() {
    GPIO_InitTypeDef GPIO_Button_Struct;
    __GPIOC_CLK_ENABLE();
    GPIO_Button_Struct.Pin = GPIO_PIN_13;
    GPIO_Button_Struct.Pull = GPIO_NOPULL;
    GPIO_Button_Struct.Mode = GPIO_MODE_IT_FALLING;
    HAL_GPIO_Init(GPIOC, &GPIO_Button_Struct);
    HAL_NVIC_SetPriority((IRQn_Type)(EXTI15_10_IRQn), 0, 0);
    HAL_NVIC_EnableIRQ((IRQn_Type)(EXTI15_10_IRQn));
}

inline void init_indicators() {
    __GPIOB_CLK_ENABLE();
    GPIO_InitTypeDef GPIO_Struct;
    GPIO_Struct.Pin = (GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 |
                       GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_10 | GPIO_PIN_13 |
                       GPIO_PIN_14 | GPIO_PIN_15);
    GPIO_Struct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_Struct.Speed = GPIO_SPEED_FAST;
    HAL_GPIO_Init(GPIOB, &GPIO_Struct);

    connector.a.port = GPIOB;
    connector.a.pin = GPIO_PIN_1;
    connector.b.port = GPIOB;
    connector.b.pin = GPIO_PIN_15;
    connector.c.port = GPIOB;
    connector.c.pin = GPIO_PIN_14;
    connector.d.port = GPIOB;
    connector.d.pin = GPIO_PIN_3;
    connector.e.port = GPIOB;
    connector.e.pin = GPIO_PIN_5;
    connector.f.port = GPIOB;
    connector.f.pin = GPIO_PIN_4;
    connector.g.port = GPIOB;
    connector.g.pin = GPIO_PIN_10;
    connector.h.port = GPIOB;
    connector.h.pin = GPIO_PIN_13;

    IndicatorPin swither1 = {GPIOB, GPIO_PIN_2};
    IndicatorPin swither2 = {GPIOB, GPIO_PIN_6};

    indicator1 = indicator_create(&connector, swither1, 0x8, 1);
    indicator2 = indicator_create(&connector, swither2, 0x8, 1);

    Indicator** indicators = (Indicator**)malloc(sizeof(Indicator*)*2);
    indicators[0] = &indicator1;
    indicators[1] = &indicator2;

    iset = indicator_create_set(2, indicators);
}

inline void init_timer() {
    __HAL_RCC_TIM4_CLK_ENABLE();

    RCC_ClkInitTypeDef sClokConfig;
    uint32_t pFLatency;
    uint32_t uwTimclock, uwAPB1Prescaler = 0;

    HAL_RCC_GetClockConfig(&sClokConfig, &pFLatency);
    uwAPB1Prescaler = sClokConfig.APB1CLKDivider;
    if (uwAPB1Prescaler == 0) {
        uwTimclock = HAL_RCC_GetPCLK1Freq();
    }
    else {
        uwTimclock = 2*HAL_RCC_GetPCLK1Freq();
    }


    Tim4_config.Instance = TIM4;
    Tim4_config.Init.Period = 99;
    Tim4_config.Init.Prescaler = (uint32_t) ((uwTimclock / 10000) - 1); // 100Mhz
    Tim4_config.Init.ClockDivision = 0;
    Tim4_config.Init.CounterMode = TIM_COUNTERMODE_UP;
    // Tim4_config.Init.RepetitionCounter = 0;

    HAL_TIM_Base_Init(&Tim4_config);

    HAL_NVIC_SetPriority(TIM4_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ((IRQn_Type)(TIM4_IRQn));

    HAL_TIM_Base_Start_IT(&Tim4_config);
}

void SPI1_IRQHandler(void)
{
    // printf("SPI1 interupt\n");
    HAL_SPI_IRQHandler(&Spi1Handle);
}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi) {
    printf("Message: ");

    uint8_t i;
    for (i=0; i<message_length; i++) {
        printf("%X ", *(message+i));
    }
    printf("\n");

    indicator_update(&indicator1, message[0], message[1]);
    indicator_update(&indicator2, message[2], message[3]);

    start_spi();
}

void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi) {

}

void EXTI15_10_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == GPIO_PIN_13) {
        printf("Button\n");
        start_spi();
    }
}

void TIM4_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&Tim4_config);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    // printf("Timer interrupt\n");
    indicator_display(&iset);
}
